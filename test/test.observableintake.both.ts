import { tap, expect } from '@push.rocks/tapbundle';

import * as smartrx from '../ts/index.js';

tap.test('should create a valid instance of observableinstake', async () => {
  const testObservableIntake = new smartrx.ObservableIntake();
  expect(testObservableIntake).toBeInstanceOf(smartrx.ObservableIntake);
});

tap.test('expect testObserservableIntake to push things', async (tools) => {
  const testObserservableIntake = new smartrx.ObservableIntake();
  testObserservableIntake.subscribe((value: any) => {
    console.log(value);
  });

  testObserservableIntake.push('hi');
  testObserservableIntake.push('wow');
  testObserservableIntake.signalComplete();
  await testObserservableIntake.completed;
});

tap.test('expect testObserservableIntake to push things', async (tools) => {
  const testObserservableIntake = new smartrx.ObservableIntake();
  testObserservableIntake.push('hi');
  testObserservableIntake.push('wow');
  testObserservableIntake.makeBuffered();
  testObserservableIntake.push('jo');
  testObserservableIntake.subscribe((value: any) => {
    console.log(value);
    testObserservableIntake.signalComplete();
  });
  testObserservableIntake.request(1);
  await testObserservableIntake.completed;
});

tap.test('', async () => {});

tap.start();
