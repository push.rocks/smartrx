/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@push.rocks/smartrx',
  version: '3.0.3',
  description: 'smart wrapper for rxjs'
}
